#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Software License Agreement (BSD License)
#
#  Copyright (c) 2014, Ocean Systems Laboratory, Heriot-Watt University, UK.
#  All rights reserved.
#
#  Redistribution and use in source and binary forms, with or without
#  modification, are permitted provided that the following conditions
#  are met:
#
#   * Redistributions of source code must retain the above copyright
#     notice, this list of conditions and the following disclaimer.
#   * Redistributions in binary form must reproduce the above
#     copyright notice, this list of conditions and the following
#     disclaimer in the documentation and/or other materials provided
#     with the distribution.
#   * Neither the name of the Heriot-Watt University nor the names of
#     its contributors may be used to endorse or promote products
#     derived from this software without specific prior written
#     permission.
#
#  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
#  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
#  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
#  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
#  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
#  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
#  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
#  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
#  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
#  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
#  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
#  POSSIBILITY OF SUCH DAMAGE.
#
#  Original authors:
#   Valerio De Carolis, Marian Andrecki, Corina Barbalata, Gordon Frost
#
#  Modified 2017, DROP Lab, university of Michigan, USA
#  Author:
#     Corina Barbalata, Eduardo Iscar, Atulya Shree


from __future__ import division

import traceback
import numpy as np


np.set_printoptions(precision=3, suppress=True)

from simulator.util import conversions as cnv
from controller import devel_cascaded_controller as cc
from simulator.model import mathematical_model as mm

import rospy
import roslib

from auv_msgs.msg import NavSts
from std_srvs.srv import Empty
from std_msgs.msg import Float64MultiArray

from vehicle_interface.msg import PilotStatus, PilotRequest, Vector6Stamped
from vehicle_interface.srv import BooleanService, FloatService
from utils.msg import ThrustersData
# general config
DEFAULT_RATE = 10.0                                     # pilot loop rate (Hz)
STATUS_RATE = 2.0                                       # pilot report rate (Hz)
TIME_REPORT = 0.5                                      # console logging (sec)

# default config
#   this values can be overridden using external configuration files
#   please see the reload_config() functions for more details
MAX_SPEED = np.array([0.8, 0.0, 0.5, 0.5, 0.0, 0.5]) # default max speed (m/s and rad/s)

MAX_THROTTLE = 0.5
MAX_THRUST = 7.86

# controller status
CTRL_DISABLED = 0
CTRL_ENABLED = 1

STATUS_CTRL = {
    CTRL_DISABLED: PilotStatus.PILOT_DISABLED,
    CTRL_ENABLED: PilotStatus.PILOT_ENABLED
}

STATUS_MODE = {
    cc.MODE_POSITION: PilotStatus.MODE_POSITION,
    cc.MODE_VELOCITY: PilotStatus.MODE_VELOCITY,
    cc.MODE_STATION: PilotStatus.MODE_STATION
}


# ros topics
TOPIC_NAV = 'nav/nav_sts'
TOPIC_THR = 'sphere_hw_iface/thruster_data'
TOPIC_FRC = 'forces/sim/body'
TOPIC_STATUS = 'pilot/status'
TOPIC_FORCES = 'pilot/forces'
TOPIC_GAINS = 'controller/gains'
TOPIC_USER = '/user/forces'

TOPIC_POS_REQ = 'pilot/position_req'
TOPIC_BODY_REQ = 'pilot/body_req'
TOPIC_VEL_REQ = 'pilot/velocity_req'
TOPIC_STAY_REQ = 'pilot/stay_req'
TOPIC_DIS_AXIS = 'user/dis_axis'

SRV_SWITCH = 'pilot/switch'
SRV_RELOAD = 'pilot/reload'
SRV_DIS_AXIS = 'pilot/disable_axis'
SRV_THRUSTERS = 'thrusters/switch'

# console output
CONSOLE_STATUS = """pilot:
  pos: %s
  vel: %s
  des_p: %s
  des_v: %s
  tau: %s
  dis: %s
"""

class VehicleControl(object):
    """VehicleControl class represent the ROS interface for the pilot subsystem.
        This class implements all the requirements to keep the vehicle within operative limits and requested
        behaviour. The pilot doesn't implement a low-level controller itself but uses one of the Controller implementations
        available in the vehicle_control module, thus making the vehicle control strategy easy to swap and separated from the main
        interfacing logic.
    """

    def __init__(self, name, pilot_rate, **kwargs):
        self.name = name
        self.pilot_rate = pilot_rate

        self.topic_output = kwargs.get('topic_output', TOPIC_FRC)

        # timing
        self.dt = 1.0 / self.pilot_rate
        self.pilot_loop = rospy.Rate(self.pilot_rate)

        self.pilot_status = rospy.Timer(rospy.Duration(1.0 / STATUS_RATE), self.send_status)
        self.ctrl_status = CTRL_ENABLED
        self.ctrl_mode = cc.MODE_POSITION

        self.disable_axis = np.zeros(6)  # works at global level
        self.max_speed = MAX_SPEED

        # pilot state
        self.pos = np.zeros(6)
        self.vel = np.zeros(6)
        self.des_pos = np.zeros(6)
        self.des_vel = np.zeros(6)
        self.err_pos = np.zeros(6)
        self.err_vel = np.zeros(6)

        # controller placeholder
        self.controller = None
        self.ctrl_type = None


        # speeds limits
        self.lim_vel_user = self.max_speed
        self.lim_vel_ctrl = self.max_speed

        # load configuration (this updates limits and modes)
        self.reload_config()

        # outputs
        self.tau_ctrl = np.zeros(6, dtype=np.float64)  # u = [X, Y, Z, K, M, N]
        self.tau = np.zeros(6, dtype=np.float64)  # u = [X, Y, Z, K, M, N]
        self.tau_user = np.zeros(6, dtype=np.float64)
        self.thr = np.zeros(4, dtype=np.float64)
        self.throttle =  np.zeros(4, dtype=np.float64)

        # limits on outputs
        self.thr_limit = MAX_THRUST*np.ones(4, dtype=np.float64)

        # thruster mapping matrix
        self.MT = np.zeros((6,4))

        # ros interface
        self.sub_nav = rospy.Subscriber(TOPIC_NAV, NavSts, self.handle_nav, tcp_nodelay=True, queue_size=1)
        self.pub_status = rospy.Publisher(TOPIC_STATUS, PilotStatus, tcp_nodelay=True, queue_size=1)
        self.pub_forces = rospy.Publisher(self.topic_output, Vector6Stamped, tcp_nodelay=True, queue_size=1)
        self.pub_thr = rospy.Publisher(TOPIC_THR, ThrustersData, tcp_nodelay=True, queue_size=1)

        # pilot requests
        self.sub_pos_req = rospy.Subscriber(TOPIC_POS_REQ, PilotRequest, self.handle_pos_req, tcp_nodelay=True, queue_size=1)
        self.sub_body_req = rospy.Subscriber(TOPIC_BODY_REQ, PilotRequest, self.handle_body_req, tcp_nodelay=True, queue_size=1)
        self.sub_vel_req = rospy.Subscriber(TOPIC_VEL_REQ, PilotRequest, self.handle_vel_req, tcp_nodelay=True, queue_size=1)
        self.sub_stay_req = rospy.Subscriber(TOPIC_STAY_REQ, PilotRequest, self.handle_stay_req, tcp_nodelay=True, queue_size=1)

        #joystick commands
        self.sub_user = rospy.Subscriber(TOPIC_USER, Vector6Stamped, self.handle_user, tcp_nodelay=True, queue_size=1)
        self.sub_dis_axis = rospy.Subscriber(TOPIC_DIS_AXIS, Vector6Stamped, self.handle_dis_axis, tcp_nodelay=True, queue_size=1)

        self.s_switch = rospy.Service(SRV_SWITCH, BooleanService, self.srv_switch)
        self.s_reload = rospy.Service(SRV_RELOAD, Empty, self.srv_reload)
       # self.s_dis = rospy.Service(SRV_DIS_AXIS, FloatService, self.srv_disable_axis)


    def reload_config(self, user_config=None):
        """This functions parses the configuration for the pilot and the low-level controller.
        It uses a configuration dictionary if provided, otherwise it queries the ROS param server for loading the latest
        version of the configuration parameters.
        :param user_config: user configuration dictionary (optional)
        """

        try:
            pilot_config = rospy.get_param('pilot', None)
        except Exception:
            tb = traceback.format_exc()
            rospy.logerr('%s: error in loading the configuration from ROS param server:\n%s', self.name, tb)
            return
        print pilot_config


        # pilot params
        self.prioritize_axis = bool(pilot_config.get('prioritize_axis', False))

        # speed limits
        self.max_speed = np.array(pilot_config.get('max_speed', MAX_SPEED.tolist()), dtype=np.float64)
        self.max_speed = np.clip(self.max_speed, -MAX_SPEED, MAX_SPEED)

        # update controller params
        self.ctrl_config = rospy.get_param('pilot/controller', dict())
        self.model_config = rospy.get_param('vehicle/model', dict())


        # controller selection (if a new controller has been requested by user)
        if self.ctrl_type != self.ctrl_config.get('type', 'cascaded'):
            # store new type
            self.ctrl_type = self.ctrl_config.get('type', 'cascaded')
            if self.ctrl_type == 'cascaded':
                self.controller = cc.CascadeController(self.dt, self.ctrl_config, self.model_config, lim_vel=self.max_speed)
            elif self.ctrl_type == 'auto':
                self.controller = cc.AutoTuneController(self.dt, self.ctrl_config, self.model_config, lim_vel=self.max_speed)
            elif self.ctrl_type == "pid":
                self.controller = cc.PIDController(self.dt, self.ctrl_config, self.model_config, lim_vel=self.max_speed)
            elif self.ctrl_type == 'mpc':
                self.controller = cc.MPCSurgeController(self.dt, self.ctrl_config, self.model_config, lim_vel=self.max_speed)
            else:
                    rospy.logfatal('controller type [%s] not supported', self.ctrl_type)
                    raise ValueError('controller type [%s] not supported', self.ctrl_type)

            # notify the selection
            rospy.loginfo('%s: enabling %s controller ...', self.name, self.ctrl_type)

            # load or reload controller configuration
            self.controller.update_config(self.ctrl_config, self.model_config)


            # safety switch service


    def _check_inputs(self):
        # position input checks:
        #   prevent negative depths (out of the water)
        #   remove roll
        #   wrap angles if necessary
        self.des_pos[2] = np.maximum(self.des_pos[2], 0)
        self.des_pos[3:6] = cnv.wrap_pi(self.des_pos[3:6])
        self.des_vel = np.clip(self.des_vel, -self.max_speed, self.max_speed)  # m/s and rad/s

        # limits input checks:
        #   prevent this go above the maximum rated speed
        self.lim_vel_user = np.clip(self.lim_vel_user, -self.max_speed, self.max_speed)
        self.lim_vel_ctrl = np.clip(self.lim_vel_ctrl, -self.max_speed, self.max_speed)



    def handle_nav(self, data):
        # parse navigation data
        self.pos = np.array([
            data.position.north,
            data.position.east,
            data.position.depth,
            data.orientation.roll,
            data.orientation.pitch,
            data.orientation.yaw
        ])

        self.vel = np.array([
            data.body_velocity.x,
            data.body_velocity.y,
            data.body_velocity.z,
            data.orientation_rate.roll,
            data.orientation_rate.pitch,
            data.orientation_rate.yaw
        ])

        # populate errors (used for info only)
        self.err_pos = self.pos - self.des_pos
        self.err_vel = self.vel - self.des_vel

    def handle_pos_req(self, data):
        try:
            # global referenced request
            self.des_pos = np.array(data.position[0:6])
            self.des_vel = np.zeros(6)
            self.lim_vel_user = self.max_speed
            self.disable_axis = np.array(data.disable_axis)


            # optionally apply speeds limits if requested by the user
            if len(data.limit_velocity) == 6:
                idx_vel = np.array(data.limit_velocity) > 0
                self.lim_vel_user[idx_vel] = np.array(data.velocity)[idx_vel]

            self._check_inputs()
            self.ctrl_mode = cc.MODE_POSITION
        except Exception:
            tb = traceback.format_exc()
            rospy.logerr('%s: bad position request\n%s', self.name, tb)

    def handle_body_req(self, data):

        try:
            # body referenced request
            J = mm.compute_jacobian(0, 0, self.pos[5])

            body_request = np.dot(J, np.array(data.position[0:6]))

            self.des_pos = self.pos + body_request
            self.des_vel = np.zeros(6)
            self.lim_vel_user = self.max_speed

            # optionally apply speeds limits if requested by the user
            if len(data.limit_velocity) == 6:
                idx_vel = np.array(data.limit_velocity) > 0
                self.lim_vel_user[idx_vel] = np.array(data.velocity)[idx_vel]

            self._check_inputs()
            self.ctrl_mode = cc.MODE_POSITION
        except Exception:
            tb = traceback.format_exc()
            rospy.logerr('%s: bad body request\n%s', self.name, tb)

    def handle_vel_req(self, data):
        try:
            self.des_pos = np.zeros(6)
            self.des_vel = np.array(data.velocity[0:6])
            self.lim_vel_user = self.max_speed
            self._check_inputs()
            self.ctrl_mode = cc.MODE_VELOCITY
        except Exception:
            tb = traceback.format_exc()
            rospy.logerr('%s: bad velocity request\n%s', self.name, tb)

    def handle_stay_req(self, data):
        self.des_pos = self.pos
        self.des_vel = np.zeros(6)

        self._check_inputs()
        self.ctrl_mode = cc.MODE_POSITION

    def handle_user(self, data):
        # read user input
        if len(data.values) == 6:
            self.tau_user = np.array(data.values)
        else:
            self.tau_user = np.zeros(6)

    def handle_dis_axis(self, data):
        self.disable_axis = np.array(data.values)


    def send_forces_thr(self):
        ns = ThrustersData()
        ns.ul = self.throttle[2]
        ns.ur = self.throttle[3]
        ns.fl = self.throttle[0]
        ns.fr = self.throttle[1]

        self.pub_thr.publish(ns)

    def send_status(self, event=None):
        ps = PilotStatus()
        ps.header.stamp = rospy.Time.now()

        ps.status = STATUS_CTRL[self.ctrl_status]
        ps.mode = STATUS_MODE[self.ctrl_mode]
        ps.des_pos = self.des_pos.tolist()
        ps.des_vel = self.des_vel.tolist()
        ps.err_pos = self.err_pos.tolist()
        ps.err_vel = self.err_vel.tolist()

        ps.lim_vel_ctrl = self.lim_vel_ctrl.tolist()
        ps.lim_vel_user = self.lim_vel_user.tolist()


        self.pub_status.publish(ps)

    # safety switch service
    def srv_switch(self, req):
        """This function handles the switch service.
        This will enable/disable the low-level controller, leaving the pilot parsing only the user input if disabled.
        Upon enabling this function reloads the configuration from the ROS param server, providing a quick way to change
        parameters in order to adjust the behaviour of the pilot or the low-level controller.
        Upon re-enabling the pilot will use the last known values for mode, desired position and velocities.
        """
        # self.ctrl_status = CTRL_ENABLED
        # return True
        if req.request is True:
            # reload configuration
            #self.reload_config()

            # enable the low-level controller
            self.ctrl_status = CTRL_ENABLED
            # reset axis disable of last request
            #   this doesn't change the global axis disable request
            #self.disable_axis_user = np.zeros(6)

            return True
        else:
            self.ctrl_status = CTRL_DISABLED
            return False

    def srv_reload(self, req):
        """This function handle the reload service.
            This will make the pilot reloading the shared configuration on-the-fly from the ROS parameter server.
            Be careful with this approach with the using with the real vehicle cause any error will reflect on the vehicle
            behaviour in terms of control, speeds, and so on ...
        """
        rospy.logwarn('%s: reloading configuration ...', self.name)
        self.reload_config()

        return []

    def load_alloc_matrix(self):
        # from cad model
        self.MT = np.array([
            [0, 0, 1, 1],
            [0, 0, 0, 0],
            [1, 1, 0, 0],
            [0.143, -0.143, 0, 0],
            [0, 0, 0, 0],
            [0, 0, 0.041, -0.041]
        ])

    def compute_thrust_throttle(self):

        self.load_alloc_matrix()  # load thruster allocation matrix

        # mapp to thrusters
        self.MT_inv = np.linalg.pinv(self.MT)
        self.thr = np.dot(self.MT_inv, self.tau)
        self.thr = np.clip(self.thr, -self.thr_limit, self.thr_limit).astype(float)

        #thrust to throttle direct mapping from graph
        # neutral position = 0.5
        # full forward = 1.0
        # full back = 0.0
        # MAX_THROTTLE = 0.5
        # MAX_THRUST = 7.86

        self.throttle = 0.5 + self.thr*MAX_THROTTLE/MAX_THRUST
        self.throttle = np.clip(self.throttle, 0, 1).astype(float)


    def loop(self):
        # init commands
        self.tau = np.zeros(6)
       # print 'Tau user ', self.tau_user

        # run the low-level control only if enabled explicitly
        if (np.all(self.disable_axis == 0)) & (self.ctrl_status == CTRL_ENABLED):
            # set controller
            self.controller.des_pos = self.des_pos
            self.controller.des_vel = self.des_vel
            self.controller.ctrl_mode = self.ctrl_mode

            #  get computed forces
            self.tau = self.controller.update(self.pos, self.vel)
            self.compute_thrust_throttle()
        # disable controller on specific axis and run together with joystick
        elif (np.any(self.disable_axis != 0)) & (self.ctrl_status == CTRL_ENABLED):
            self.controller.des_pos = self.des_pos
            self.controller.des_vel = self.des_vel
            self.controller.ctrl_mode = self.ctrl_mode

            #  get computed forces
            self.tau = self.controller.update(self.pos, self.vel)
            idx_dis = np.where(self.disable_axis == 1)
            self.tau[idx_dis] = 0
            self.tau = self.tau + self.tau_user

            self.compute_thrust_throttle()
        # full joystick control
        elif (np.any(self.tau_user != 0)) & (self.ctrl_status == CTRL_DISABLED):
            self.tau = self.tau_user
            self.compute_thrust_throttle()


        # send force feedback
        pf = Vector6Stamped()
        pf.header.stamp = rospy.Time.now()
        pf.values = self.tau.tolist()
        self.pub_forces.publish(pf)

        self.send_forces_thr()


    def report_status(self, event=None):

        if self.ctrl_status == CTRL_ENABLED:
            print(self.controller)

    def run(self):
        # init pilot
        rospy.loginfo('%s: pilot initialized ...', self.name)

        import time

        # pilot loop
        while not rospy.is_shutdown():
            # t_start = time.time()

            # run main pilot code
            self.loop()

            # t_end = time.time()
            # rospy.loginfo('%s: loop time: %.3f s', self.name, t_end - t_start)

            try:
                self.pilot_loop.sleep()
            except rospy.ROSInterruptException:
                rospy.loginfo('%s shutdown requested ...', self.name)

        # graceful shutdown
        self.ctrl_status = CTRL_DISABLED

    def __str__(self):
        return CONSOLE_STATUS % (
            self.pos, self.vel, self.des_pos, self.des_vel,
            self.tau,
            self.disable_axis
        )


def main():
    rospy.init_node('vehicle_pilot')
    name = rospy.get_name()
    rospy.loginfo('%s initializing ...', name)

    # load global parameters
    rate = int(rospy.get_param('~pilot_rate', DEFAULT_RATE))
    topic_output = rospy.get_param('~topic_output', TOPIC_FRC)
    verbose = bool(rospy.get_param('~verbose', False))

    rate = int(np.clip(rate, 1, 100).astype(int))

    # show current settings
    rospy.loginfo('%s pilot rate: %s Hz', name, rate)
    rospy.loginfo('%s topic output: %s', name, topic_output)

    # start vehicle control node
    pilot = VehicleControl(name, rate, topic_output=topic_output, verbose=verbose)

    try:
        pilot.run()
    except Exception:
        tb = traceback.format_exc()
        rospy.logfatal('%s uncaught exception, dying!\n%s', name, tb)



if __name__ == '__main__':
    main()








