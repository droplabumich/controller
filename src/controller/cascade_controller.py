#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Software License Agreement (BSD License)
#
#  Copyright (c) 2014, Ocean Systems Laboratory, Heriot-Watt University, UK.
#  All rights reserved.
#
#  Redistribution and use in source and binary forms, with or without
#  modification, are permitted provided that the following conditions
#  are met:
#
#   * Redistributions of source code must retain the above copyright
#     notice, this list of conditions and the following disclaimer.
#   * Redistributions in binary form must reproduce the above
#     copyright notice, this list of conditions and the following
#     disclaimer in the documentation and/or other materials provided
#     with the distribution.
#   * Neither the name of the Heriot-Watt University nor the names of
#     its contributors may be used to endorse or promote products
#     derived from this software without specific prior written
#     permission.
#
#  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
#  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
#  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
#  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
#  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
#  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
#  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
#  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
#  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
#  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
#  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
#  POSSIBILITY OF SUCH DAMAGE.
#
#  Original authors:
#   Valerio De Carolis, Marian Andrecki, Corina Barbalata, Gordon Frost
#
#  Modified 2018, DROP Lab, university of Michigan, USA
#  Author:
#     Corina Barbalata, Eduardo Iscar, Atulya Shree

from __future__ import division

import numpy as np
#import libraries.mpctools as mpc
import rospy
np.set_printoptions(precision=3, suppress=True)

from simulator.model import vehicle_model as vm
from simulator.model import mathematical_model as mm
from simulator.util import conversions as cnv


# controller modes
MODE_POSITION = 0
MODE_VELOCITY = 1
MODE_STATION = 2

CONSOLE_STATUS = """controller:
  pos: %s
  des_p: %s
  vel: %s
  des_v: %s
  mode: %s
"""

class Controller(object):
    """Controller class wraps the low-level controllers for velocity and position.
    This class can be used as a parent class used to describe the behaviour of a different vehicle controllers.
    """

    def __init__(self, dt, config, **kwargs):
        # config
        self.dt = dt
        self.config = config

        # mode
        self.ctrl_mode = MODE_POSITION

        # states
        self.pos = np.zeros(6)
        self.vel = np.zeros(6)

        # requests
        self.des_pos = np.zeros(6)
        self.des_vel = np.zeros(6)

        # limits
        self.lim_vel = kwargs.get('lim_vel', 10 * np.ones(6))

    def update_config(self, ctrl_config, model_config):
        pass

    def update(self, position, velocity):
        return np.zeros(6)

    def __str__(self):
        return CONSOLE_STATUS % (
            self.pos, self.des_pos, self.vel, self.des_vel, self.ctrl_mode
        )

class PIDController(Controller):
    """PID controller implemnts the control structure for position.
           The output of this class is the force necessary for the 6 DOFs to move the vehicle so that the required
           characteristics are achieved.
        """

    def __init__(self, dt, ctrl_config, model_config, **kwargs):
        super(PIDController, self).__init__(dt, ctrl_config, **kwargs)

        # init params
        # position gains
        self.pos_Kp = np.zeros(6)
        self.pos_Kd = np.zeros(6)
        self.pos_Ki = np.zeros(6)

        # controller limits
        self.pos_lim = np.zeros(6)
        self.tau = np.zeros(6)

        # errors
        self.err_pos = np.zeros(6)
        self.err_pos_prev = np.zeros(6)
        self.err_pos_der = np.zeros(6)
        self.err_pos_int = np.zeros(6)

        # init jacobians matrices
        self.J = np.zeros((6, 6))  # jacobian matrix (translate velocity from body referenced to Earth referenced)
        self.J_inv = np.zeros((6, 6))  # inverse jacobian matrix

    def update_config(self, ctrl_config, model_config):
        # pid parameters (position)
        self.pos_Kp = np.array([
            ctrl_config['pos_x']['kp'],
            ctrl_config['pos_y']['kp'],
            ctrl_config['pos_z']['kp'],
            ctrl_config['pos_k']['kp'],
            ctrl_config['pos_m']['kp'],
            ctrl_config['pos_n']['kp'],
        ])

        self.pos_Kd = np.array([
            ctrl_config['pos_x']['kd'],
            ctrl_config['pos_y']['kd'],
            ctrl_config['pos_z']['kd'],
            ctrl_config['pos_k']['kd'],
            ctrl_config['pos_m']['kd'],
            ctrl_config['pos_n']['kd'],
        ])

        self.pos_Ki = np.array([
            ctrl_config['pos_x']['ki'],
            ctrl_config['pos_y']['ki'],
            ctrl_config['pos_z']['ki'],
            ctrl_config['pos_k']['ki'],
            ctrl_config['pos_m']['ki'],
            ctrl_config['pos_n']['ki'],
        ])

        self.pos_lim = np.array([
            ctrl_config['pos_x']['lim'],
            ctrl_config['pos_y']['lim'],
            ctrl_config['pos_z']['lim'],
            ctrl_config['pos_k']['lim'],
            ctrl_config['pos_m']['lim'],
            ctrl_config['pos_n']['lim'],
        ])


    def update(self, position, velocity):
        # store nav updates
        self.pos = position
        self.vel = velocity

        # update jacobians
        self.J = mm.update_jacobian(self.J, self.pos[3], self.pos[4], self.pos[5])
        self.J_inv = np.linalg.pinv(self.J)

        # model-free pid cascaded controller
        #   first pid (outer loop on position)
        self.err_pos = self.pos - self.des_pos
        # self.err_pos = np.dot(self.J_inv, self.err_pos.reshape((6, 1))).flatten()
        # wrap angles and limit pitch
        self.err_pos[3:6] = cnv.wrap_pi(self.err_pos[3:6])

        # update errors
        self.err_pos_der = (self.err_pos - self.err_pos_prev) / self.dt
        self.err_pos_int = np.clip(self.err_pos_int + self.err_pos, -self.pos_lim, self.pos_lim)

        # Position integral terms set to zero to avoid oscillations
        # pos_changed = np.sign(self.err_pos) != np.sign(self.err_pos_prev)
        # pos_changed[2] = False
        # self.err_pos_int[pos_changed] = 0.0

        self.err_pos_prev = self.err_pos

        # first pid output (plus speed limits if requested by the user)
        self.tau = (-self.pos_Kp * self.err_pos) + (-self.pos_Kd * self.err_pos_der) + ( -self.pos_Ki * self.err_pos_int)

        # these are set to zero only for depth control testing -- comment the following 5 lines if full control is active
        # self.tau[4] = 0.0
        # self.tau[0] = 0.0
        # self.tau[1] = 0.0
        # self.tau[3] = 0.0
        # self.tau[5] = 0.0

        return self.tau

    def __str__(self):
        return """%s
                 ep: %s
                 ed: %s
                 ei: %s
                 tau_c: %s
               """ % (
            super(PIDController, self).__str__(),
            self.err_pos, self.err_pos_der, self.err_pos_int, self.tau
        )


class CascadeController(Controller):
    """CascadeController implements a cascaded PID controllers for position and velocity.
       The output of this class is the force necessary for the 6 DOFs to move the vehicle so that the required
       characteristics are achieved.
    """

    def __init__(self, dt, ctrl_config, model_config, **kwargs):
        super(CascadeController, self).__init__(dt, ctrl_config, **kwargs)

        # init params
        # position gains
        self.pos_Kp = np.zeros(6)
        self.pos_Kd = np.zeros(6)
        self.pos_Ki = np.zeros(6)

        # velocity gains
        self.vel_Kp = np.zeros(6)
        self.vel_Kd = np.zeros(6)
        self.vel_Ki = np.zeros(6)

        # controller limits
        self.pos_lim = np.zeros(6)
        self.vel_lim = np.zeros(6)

        self.req_vel = np.zeros(6)
        self.tau_ctrl = np.zeros(6)
        self.tau_prev = np.zeros(6)

        # errors
        self.err_pos = np.zeros(6)
        self.err_pos_prev = np.zeros(6)
        self.err_pos_der = np.zeros(6)
        self.err_pos_int = np.zeros(6)
        self.err_vel = np.zeros(6)
        self.err_vel_prev = np.zeros(6)
        self.err_vel_der = np.zeros(6)
        self.err_vel_int = np.zeros(6)

        # init jacobians matrices
        self.J = np.zeros((6, 6))  # jacobian matrix (translate velocity from body referenced to Earth referenced)
        self.J_inv = np.zeros((6, 6))  # inverse jacobian matrix

    def update_config(self, ctrl_config, model_config):
        # main control loop

        # pid parameters (position)
        self.pos_Kp = np.array([
                ctrl_config['pos_x']['kp'],
                ctrl_config['pos_y']['kp'],
                ctrl_config['pos_z']['kp'],
                ctrl_config['pos_k']['kp'],
                ctrl_config['pos_m']['kp'],
                ctrl_config['pos_n']['kp'],
        ])

        self.pos_Kd = np.array([
                ctrl_config['pos_x']['kd'],
                ctrl_config['pos_y']['kd'],
                ctrl_config['pos_z']['kd'],
                ctrl_config['pos_k']['kd'],
                ctrl_config['pos_m']['kd'],
                ctrl_config['pos_n']['kd'],
        ])

        self.pos_Ki = np.array([
                ctrl_config['pos_x']['ki'],
                ctrl_config['pos_y']['ki'],
                ctrl_config['pos_z']['ki'],
                ctrl_config['pos_k']['ki'],
                ctrl_config['pos_m']['ki'],
                ctrl_config['pos_n']['ki'],
        ])

        self.pos_lim = np.array([
                ctrl_config['pos_x']['lim'],
                ctrl_config['pos_y']['lim'],
                ctrl_config['pos_z']['lim'],
                ctrl_config['pos_k']['lim'],
                ctrl_config['pos_m']['lim'],
                ctrl_config['pos_n']['lim'],
        ])

        # pid parameters (velocity)
        self.vel_Kp = np.array([
                ctrl_config['vel_u']['kp'],
                ctrl_config['vel_v']['kp'],
                ctrl_config['vel_w']['kp'],
                ctrl_config['vel_p']['kp'],
                ctrl_config['vel_q']['kp'],
                ctrl_config['vel_r']['kp'],
        ])

        self.vel_Kd = np.array([
                ctrl_config['vel_u']['kd'],
                ctrl_config['vel_v']['kd'],
                ctrl_config['vel_w']['kd'],
                ctrl_config['vel_p']['kd'],
                ctrl_config['vel_q']['kd'],
                ctrl_config['vel_r']['kd'],
        ])

        self.vel_Ki = np.array([
                ctrl_config['vel_u']['ki'],
                ctrl_config['vel_v']['ki'],
                ctrl_config['vel_w']['ki'],
                ctrl_config['vel_p']['ki'],
                ctrl_config['vel_q']['ki'],
                ctrl_config['vel_r']['ki'],
        ])

        self.vel_lim = np.array([
                ctrl_config['vel_u']['lim'],
                ctrl_config['vel_v']['lim'],
                ctrl_config['vel_w']['lim'],
                ctrl_config['vel_p']['lim'],
                ctrl_config['vel_q']['lim'],
                ctrl_config['vel_r']['lim'],
        ])

        self.vel_input_lim = np.array([
                ctrl_config['vel_u']['input_lim'],
                ctrl_config['vel_v']['input_lim'],
                ctrl_config['vel_w']['input_lim'],
                ctrl_config['vel_p']['input_lim'],
                ctrl_config['vel_q']['input_lim'],
                ctrl_config['vel_r']['input_lim'],
        ])


    def update(self, position, velocity):
        # main control loop

        # store nav updates
        self.pos = position
        self.vel = velocity

        # update jacobians
        self.J = mm.update_jacobian(self.J, self.pos[3], self.pos[4], self.pos[5])
        self.J_inv = np.linalg.pinv(self.J)

        # model-free pid cascaded controller
        #   first pid (outer loop on position)
        self.err_pos = self.pos - self.des_pos
        self.err_pos = np.dot(self.J_inv, self.err_pos.reshape((6, 1))).flatten()
        # wrap angles and limit pitch
        self.err_pos[3:6] = cnv.wrap_pi(self.err_pos[3:6])

        # update errors
        self.err_pos_der = (self.err_pos - self.err_pos_prev) / self.dt
        self.err_pos_int = np.clip(self.err_pos_int + self.err_pos, -self.pos_lim, self.pos_lim)

        # Position integral terms set to zero to avoid oscillations
        pos_changed = np.sign(self.err_pos) != np.sign(self.err_pos_prev)
        pos_changed[2] = False
        self.err_pos_int[pos_changed] = 0.0

        # update previous error
        self.err_pos_prev = self.err_pos

        # first pid output (plus speed limits if requested by the user)
        self.req_vel = (-self.pos_Kp * self.err_pos) + (-self.pos_Kd * self.err_pos_der) + (-self.pos_Ki * self.err_pos_int)


        # if running in velocity mode ignore the first pid
        if self.ctrl_mode == MODE_VELOCITY:
            self.req_vel = self.des_vel

        # apply user velocity limits (if any)
        self.req_vel = np.clip(self.req_vel, -self.lim_vel, self.lim_vel)

        # model-free pid cascaded controller
        #   second pid (inner loop on velocity)
        self.err_vel = np.clip(self.vel - self.req_vel, -self.vel_input_lim, self.vel_input_lim)
        self.err_vel_der = (self.err_vel - self.err_vel_prev) / self.dt
        self.err_vel_int = np.clip(self.err_vel_int + self.err_vel, -self.vel_lim, self.vel_lim)

        # velocity integral terms set to zero to avoid oscillations
        # vel_changed = np.sign(self.err_vel) != np.sign(self.err_vel_prev)
        # vel_changed[2] = False
        # self.err_vel_int[vel_changed] = 0.0

        # update previous error
        self.err_vel_prev = self.err_vel

        # second pid output
        self.tau = (-self.vel_Kp * self.err_vel) + (-self.vel_Kd * self.err_vel_der) + (-self.vel_Ki * self.err_vel_int)
        # self.tau[1] = 0.0
        # self.tau[3] = 0.0


        self.tau_prev = self.tau

        return self.tau

    def __str__(self):

        return """%s
             req_v: %s
             lim_v: %s
             ep: %s
             ed: %s
             ei: %s
             evp: %s
             evd: %s
             evi: %s
             tau_c: %s
           """ % (
            super(CascadeController, self).__str__(),
            self.req_vel, self.lim_vel,
            self.err_pos, self.err_pos_der, self.err_pos_int,
            self.err_vel, self.err_vel_der, self.err_vel_int,
            self.tau_ctrl
        )



class AutoTuneController(CascadeController):
    """AutoTuneController implements an auto tuning controllers for position and velocity.
       The output of this class is the force necessary for the 6 DOFs to move the vehicle so that the required
       characteristics are achieved.
    """

    def __init__(self, dt, ctrl_config, model_config, **kwargs):
        super(AutoTuneController, self).__init__(dt, ctrl_config, model_config, **kwargs)

        # init params
        # adaption coefficients for each DOF of vehicle
        self.adapt_coeff_pos = np.zeros(6)  # position
        self.adapt_coeff_vel = np.zeros(6)  # velocity
        self.adapt_limit_pos = np.zeros(3)
        self.adapt_limit_vel = np.zeros(3)
        self.pitch_surge_coeff = 0.0
        self.pitch_rest_coeff = 0.0
        self.tau_ctrl_prev = np.zeros(6)

    def update_config(self, ctrl_config, model_config):
        # load parameters from default controller
        super(AutoTuneController, self).update_config(ctrl_config, model_config)

        # adaptation coefficients
        self.adapt_coeff_pos = np.array([
            ctrl_config['adapt_coeff_pos']['x'],
            ctrl_config['adapt_coeff_pos']['y'],
            ctrl_config['adapt_coeff_pos']['z'],
            ctrl_config['adapt_coeff_pos']['k'],
            ctrl_config['adapt_coeff_pos']['m'],
            ctrl_config['adapt_coeff_pos']['n'],
        ])

        self.adapt_coeff_vel = np.array([
            ctrl_config['adapt_coeff_vel']['u'],
            ctrl_config['adapt_coeff_vel']['v'],
            ctrl_config['adapt_coeff_vel']['w'],
            ctrl_config['adapt_coeff_vel']['p'],
            ctrl_config['adapt_coeff_vel']['q'],
            ctrl_config['adapt_coeff_vel']['r'],
        ])

        self.adapt_limit_pos = np.array([
            ctrl_config['adapt_limit_pos']['p'],
            ctrl_config['adapt_limit_pos']['i'],
            ctrl_config['adapt_limit_pos']['d']
        ])

        self.adapt_limit_vel = np.array([
            ctrl_config['adapt_limit_vel']['p'],
            ctrl_config['adapt_limit_vel']['i'],
            ctrl_config['adapt_limit_vel']['d']
        ])

        # pitch controller parameters
        self.pitch_surge_coeff = float(ctrl_config.get('pitch_surge_coeff', 0.0))
        self.pitch_rest_coeff = float(ctrl_config.get('pitch_rest_coeff', 0.0))

    def update(self, position, velocity):
        # store nav updates
        self.pos = position
        self.vel = velocity

        # update jacobians
        self.J = mm.update_jacobian(self.J, self.pos[3], self.pos[4], self.pos[5])
        self.J_inv = np.linalg.pinv(self.J)

        #   first pid (outer loop on position)
        self.err_pos = self.pos - self.des_pos

        # wrap angles and limit pitch
        self.err_pos[3:6] = cnv.wrap_pi(self.err_pos[3:6])

        # update errors
        self.err_pos_der = (self.err_pos - self.err_pos_prev) / self.dt
        self.err_pos_int = np.clip(self.err_pos_int + self.err_pos, -self.pos_lim, self.pos_lim)

        # adaptive tuning of position gains
        self.pos_Kp += self.adapt_coeff_pos * self.err_pos * np.abs(self.err_pos)
        self.pos_Ki += self.adapt_coeff_pos * self.err_pos * self.err_pos_int
        self.pos_Kd += self.adapt_coeff_pos * self.err_pos * self.err_pos_der

        self.pos_Kp = np.clip(self.pos_Kp, -self.adapt_limit_pos[0], self.adapt_limit_pos[0])
        self.pos_Ki = np.clip(self.pos_Ki, -self.adapt_limit_pos[1], self.adapt_limit_pos[1])
        self.pos_Kd = np.clip(self.pos_Kd, -self.adapt_limit_pos[2], self.adapt_limit_pos[2])

        # update previous error
        self.err_pos_prev = self.err_pos

        # Position integral terms set to zero to avoid oscillations
        pos_changed = np.sign(self.err_pos) != np.sign(self.err_pos_prev)
        pos_changed[2] = False
        self.err_pos_int[pos_changed] = 0.0

        # first pid output (plus speed limits if requested by the user)
        self.req_vel = (-self.pos_Kp * self.err_pos) + (-self.pos_Kd * self.err_pos_der) + (-self.pos_Ki * self.err_pos_int)

        # if running in velocity mode ignore the first pid
        if self.ctrl_mode == MODE_VELOCITY:
            self.req_vel = self.des_vel

        # apply user velocity limits (if any)
        self.req_vel = np.clip(self.req_vel, -self.lim_vel, self.lim_vel)

        #  second pid (inner loop on velocity)
        self.err_vel = np.clip(self.vel - self.req_vel, -self.vel_input_lim, self.vel_input_lim)
        self.err_vel_der = (self.err_vel - self.err_vel_prev) / self.dt
        self.err_vel_int = np.clip(self.err_vel_int + self.err_vel, -self.vel_lim, self.vel_lim)

        # velocity integral terms set to zero to avoid oscillations
        vel_changed = np.sign(self.err_vel) != np.sign(self.err_vel_prev)
        vel_changed[2] = False
        self.err_vel_int[vel_changed] = 0.0

        # update previous error
        self.err_vel_prev = self.err_vel

        # adaptive tuning of velocity gains
        self.vel_Kp += self.adapt_coeff_vel * self.err_vel * np.abs(self.err_vel)
        self.vel_Ki += self.adapt_coeff_vel * self.err_vel * self.err_vel_int
        self.vel_Kd += self.adapt_coeff_vel * self.err_vel * self.err_vel_der

        self.vel_Kp = np.clip(self.vel_Kp, -self.adapt_limit_vel[0], self.adapt_limit_vel[0])
        self.vel_Ki = np.clip(self.vel_Ki, -self.adapt_limit_vel[1], self.adapt_limit_vel[1])
        self.vel_Kd = np.clip(self.vel_Kd, -self.adapt_limit_vel[2], self.adapt_limit_vel[2])

        # Velocity integral terms set to zero to avoid oscillations
        vel_changed = np.sign(self.err_vel) != np.sign(self.err_vel_prev)
        vel_changed[2] = False  # ignore the depth

        self.err_vel_int[vel_changed] = 0.0

        # second pid output
        self.tau = (-self.vel_Kp * self.err_vel) + (-self.vel_Kd * self.err_vel_der) + (-self.vel_Ki * self.err_vel_int)
	print "Force in yaw ", self.tau[5], " error ",self.err_pos[5]

        self.tau_prev = self.tau

        return self.tau

    def __str__(self):
        return """%s
                 pos_kp: %s
                 pos_kd: %s
                 pos_ki: %s
                 vel_kp: %s
                 vel_kd: %s
                 vel_ki: %s
               """ % (
            super(AutoTuneController, self).__str__(),
            self.pos_Kp, self.pos_Kd, self.pos_Ki,
            self.vel_Kp, self.vel_Kd, self.vel_Ki,
        )

class SlidingModeController(Controller):
    """SlidingModeController implements a sliding mode controller for .
           The output of this class is the force necessary for the 6 DOFs to move the vehicle so that the required
           characteristics are achieved.
    """
    def __init__(self, dt, ctrl_config, model_config, **kwargs):
        super(SlidingModeController, self).__init__(dt, ctrl_config, **kwargs)

        # init params
        # position gains
        self.epsilon = np.zeros(6)
        self.k = np.zeros(6)
        self.sigma = np.zeros(6)
        self.limit = np.zeros(6)

        # controller limits
        self.pos_lim = np.zeros(6)

        self.tau_ctrl = np.zeros(6)
        self.tau_prev = np.zeros(6)

        # errors
        self.err_pos = np.zeros(6)
        self.err_pos_prev = np.zeros(6)
        self.err_pos_der = np.zeros(6)
	self.err_pos_int = np.zeros(6)

        # init jacobians matrices
        self.J = np.zeros((6, 6))  # jacobian matrix (translate velocity from body referenced to Earth referenced)
        self.J_inv = np.zeros((6, 6))  # inverse jacobian matrix

    def update_config(self, ctrl_config, model_config):
        # sliding mode parameters
        self.epsilon = np.array([
            ctrl_config['pos_x']['epsilon'],
            ctrl_config['pos_y']['epsilon'],
            ctrl_config['pos_z']['epsilon'],
            ctrl_config['pos_k']['epsilon'],
            ctrl_config['pos_m']['epsilon'],
            ctrl_config['pos_n']['epsilon'],
        ])

        self.k = np.array([
            ctrl_config['pos_x']['k'],
            ctrl_config['pos_y']['k'],
            ctrl_config['pos_z']['k'],
            ctrl_config['pos_k']['k'],
            ctrl_config['pos_m']['k'],
            ctrl_config['pos_n']['k'],
        ])

        self.limit = np.array([
            ctrl_config['pos_x']['lim'],
            ctrl_config['pos_y']['lim'],
            ctrl_config['pos_z']['lim'],
            ctrl_config['pos_k']['lim'],
            ctrl_config['pos_m']['lim'],
            ctrl_config['pos_n']['lim'],
        ])

    def sigmoid(self, x):
        "Numerically-stable sigmoid function."

        return np.where(x>=0,1/(1+np.exp(-x))-0.5, np.exp(x)/(1+np.exp(x))-0.5)
        #if np.all(x) >= 0:
        #    z = np.exp(-x)
        #    return 1 / (1 + z)
        #else:
        #    z = np.exp(x)
        #return z / (1 + z)

    def update(self, position, velocity):
        # store nav updates
        self.pos = position
        self.err_pos = self.des_pos - self.pos
        self.err_pos[3:6] = cnv.wrap_pi(self.err_pos[3:6])
        # update errors
        self.err_pos_der = (self.err_pos - self.err_pos_prev) / self.dt
        self.err_pos_int = np.clip(self.err_pos_int + self.err_pos, -self.limit, self.limit)
        self.sigma = self.err_pos_der + self.k*self.err_pos
        print "Sigma: ",self.sigma
        self.tau = - self.epsilon*self.sigmoid(self.sigma)
        self.tau[5] = -self.k[5]*self.err_pos[5] - self.epsilon[5]*self.err_pos_int[5]
        print "Force in yaw ", self.tau[5], " error in yaw ", self.err_pos[5]
        return self.tau

    def __str__(self):
        return """%s
                epsilon: %s
                k: %s
                limit: %s
                """ % (
            super(SlidingModeController, self).__str__(),
            self.epsilon, self.k, self.limit,
        )

class MPCSurgeController(Controller):
        #  When running this controller add this at the begining of this file: from mpctools import mpc
        """MPCSurge controller implements a 1 DOF surge controller for the vehicle
        """

        def __init__(self, dt, ctrl_config, model_config, **kwargs):
            super(MPCSurgeController, self).__init__(dt, ctrl_config, **kwargs)

            # init params
            # position gains
            self.pos_Kp = np.zeros(6)
            self.pos_Kd = np.zeros(6)
            self.pos_Ki = np.zeros(6)

            # velocity gains
            self.vel_Kp = np.zeros(6)
            self.vel_Kd = np.zeros(6)
            self.vel_Ki = np.zeros(6)

            # controller limits
            self.pos_lim = np.zeros(6)
            self.vel_lim = np.zeros(6)

            self.req_vel = np.zeros(6)
            self.tau_ctrl = np.zeros(6)
            self.tau_prev = np.zeros(6)

            # errors
            self.err_pos = np.zeros(6)
            self.err_pos_prev = np.zeros(6)
            self.err_pos_der = np.zeros(6)
            self.err_pos_int = np.zeros(6)
            self.err_vel = np.zeros(6)
            self.err_vel_prev = np.zeros(6)
            self.err_vel_der = np.zeros(6)
            self.err_vel_int = np.zeros(6)

            # init jacobians matrices
            self.J = np.zeros((6, 6))  # jacobian matrix (translate velocity from body referenced to Earth referenced)
            self.J_inv = np.zeros((6, 6))  # inverse jacobian matrix

            # Some constants.
            self.Delta = .1
            self.Nx = 2
            self.Nu = 1


            #### MPC controller
            self.current_velocity = np.zeros(6)
            self.x0_pose = np.zeros(6)
            self.x0_vel = np.array([0.00001, 0.0, 0.0, 0.0, 0.0, 0.0])

        def update_config(self, ctrl_config, model_config):
            # pid parameters (position)
            self.pos_Kp = np.array([
                ctrl_config['pos_x']['kp'],
                ctrl_config['pos_y']['kp'],
                ctrl_config['pos_z']['kp'],
                ctrl_config['pos_k']['kp'],
                ctrl_config['pos_m']['kp'],
                ctrl_config['pos_n']['kp'],
            ])

            self.pos_Kd = np.array([
                ctrl_config['pos_x']['kd'],
                ctrl_config['pos_y']['kd'],
                ctrl_config['pos_z']['kd'],
                ctrl_config['pos_k']['kd'],
                ctrl_config['pos_m']['kd'],
                ctrl_config['pos_n']['kd'],
            ])

            self.pos_Ki = np.array([
                ctrl_config['pos_x']['ki'],
                ctrl_config['pos_y']['ki'],
                ctrl_config['pos_z']['ki'],
                ctrl_config['pos_k']['ki'],
                ctrl_config['pos_m']['ki'],
                ctrl_config['pos_n']['ki'],
            ])

            self.pos_lim = np.array([
                ctrl_config['pos_x']['lim'],
                ctrl_config['pos_y']['lim'],
                ctrl_config['pos_z']['lim'],
                ctrl_config['pos_k']['lim'],
                ctrl_config['pos_m']['lim'],
                ctrl_config['pos_n']['lim'],
            ])

            # pid parameters (velocity)
            self.vel_Kp = np.array([
                ctrl_config['vel_u']['kp'],
                ctrl_config['vel_v']['kp'],
                ctrl_config['vel_w']['kp'],
                ctrl_config['vel_p']['kp'],
                ctrl_config['vel_q']['kp'],
                ctrl_config['vel_r']['kp'],
            ])

            self.vel_Kd = np.array([
                ctrl_config['vel_u']['kd'],
                ctrl_config['vel_v']['kd'],
                ctrl_config['vel_w']['kd'],
                ctrl_config['vel_p']['kd'],
                ctrl_config['vel_q']['kd'],
                ctrl_config['vel_r']['kd'],
            ])

            self.vel_Ki = np.array([
                ctrl_config['vel_u']['ki'],
                ctrl_config['vel_v']['ki'],
                ctrl_config['vel_w']['ki'],
                ctrl_config['vel_p']['ki'],
                ctrl_config['vel_q']['ki'],
                ctrl_config['vel_r']['ki'],
            ])

            self.vel_lim = np.array([
                ctrl_config['vel_u']['lim'],
                ctrl_config['vel_v']['lim'],
                ctrl_config['vel_w']['lim'],
                ctrl_config['vel_p']['lim'],
                ctrl_config['vel_q']['lim'],
                ctrl_config['vel_r']['lim'],
            ])

            self.vel_input_lim = np.array([
                ctrl_config['vel_u']['input_lim'],
                ctrl_config['vel_v']['input_lim'],
                ctrl_config['vel_w']['input_lim'],
                ctrl_config['vel_p']['input_lim'],
                ctrl_config['vel_q']['input_lim'],
                ctrl_config['vel_r']['input_lim'],
            ])

        def update(self, position, velocity):
            self.tau = np.zeros(6)
            self.tau[0] = self.NMPC_Controller()
            return self.tau

        def NMPC_Controller(self):
            time_begin = rospy.get_rostime()
            # Parameter Updates
            v = self.vel[1]
            w = self.vel[2]
            p = self.vel[3]
            q = self.vel[4]
            r = self.vel[5]
            phi = self.pos[3]
            theta = self.pos[4]
            psi = self.pos[5]

            # Vehicle parameters
            zg = 0.00178  # Vertical center of gravity
            m = 20.42  # Mass of the vehicle
            W = 200.9   # Weight of vehicle
            B = 201.1  # Buoyancy of vehicle
            Xu_dot = -2


            Fu = -(W - B) * np.sin(theta) - (-v * r + w * q + zg * p * r) * m
            M_mass_v = m - Xu_dot
            Fpos = (-np.sin(psi) * np.cos(phi) + np.cos(psi) * np.sin(theta) * np.sin(phi)) * v + (np.sin(psi) * np.sin(
                phi) + np.cos(psi) * np.sin(theta) * np.cos(phi)) * w
            Kpos = np.cos(psi) * np.cos(theta)
            Dq = 48.17
            Dl = 0.0

            # Define model and get simulator.
            def ode(x, u):
                return np.array([(Fu - (Dq * x[0] + Dl) * x[0] + u[0]) / M_mass_v, Kpos * x[0] + Fpos])

            # Then get nonlinear casadi functions and the linearization.
            ode_casadi = mpc.getCasadiFunc(ode, [self.Nx, self.Nu], ["x", "u"], funcname="f")

            # Define stage cost and terminal weight.
            def lfunc(x, u):
                Js = 2 * np.power((u / 2), 2) * 0.1
                return Js


            l = mpc.getCasadiFunc(lfunc, [self.Nx, self.Nu], ["x", "u"], funcname="l")

            def Pffunc(x):
                Je = (self.des_pos[0] - x[1]) * (2 * np.power((0.2 / 2), 2) + 2 * np.power((Dq * x[0] + Dl) * x[0] / 2, 2)) / (
                x[0] + self.current_velocity[0])
                return Je

            Pf = mpc.getCasadiFunc(Pffunc, [self.Nx], ["x"], funcname="Pf")

            # Make optimizers. Note that the linear and nonlinear solvers have some common
            # arguments, so we collect those below.
            Nt = 5
            # Specify bounds.
            xlb = np.array([0, -np.inf])
            xub = np.array([1, np.inf])
            ulb = np.array([-15.72])
            uub = np.array([15.72])

            lb = {
                "u": np.tile(ulb, (Nt, 1)),
                "x": np.tile(xlb, (Nt + 1, 1)),
            }
            ub = {
                "u": np.tile(uub, (Nt, 1)),
                "x": np.tile(xub, (Nt + 1, 1)),
            }

            commonargs = dict(
                verbosity=0,
                l=l,
                x0=[self.x0_vel[0], self.x0_pose[0]],
                Pf=Pf,
                lb=lb,
                ub=ub,
            )
            Nnonlin = {"t": Nt, "x": self.Nx, "u": self.Nu}
            Nnonlin["c"] = 2  # Use collocation to discretize.

            solvers = mpc.nmpc(f=ode_casadi, N=Nnonlin, Delta=self.Delta, **commonargs)
            solvers.fixvar("x", 0, [self.x0_vel[0], self.x0_pose[0]])
            solvers.solve()
            self.x0_pose = self.pos
            self.x0_vel = self.vel
            now = time_begin - rospy.get_rostime()
            rospy.loginfo("Current time %i %i", now.secs, now.nsecs)
            return solvers.var["u", 0]



        def __str__(self):

            return """%s
                 req_v: %s
                 lim_v: %s
                 ep: %s
                 ed: %s
                 ei: %s
                 evp: %s
                 evd: %s
                 evi: %s
                 tau_c: %s
               """ % (
                super(MPCSurgeController, self).__str__(),
                self.req_vel, self.lim_vel,
                self.err_pos, self.err_pos_der, self.err_pos_int,
                self.err_vel, self.err_vel_der, self.err_vel_int,
                self.tau_ctrl
            )













